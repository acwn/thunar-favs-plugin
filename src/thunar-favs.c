/*

Filename: thunar-favs.c
Copyright  2005 - 2020  Alexander Nagel
Email: filemanager-avs@acwn.de
Homepage: http://www.acwn.de/projects/filemanager-avs

This file is part of Filemanager-AVS Thunar plugin.

*/

/* Headers */
#include "thunar-favs.h"

/* function for files in datadir */
static const gchar* avs_get_full_location (gchar* filename)
{
  return g_build_filename (DATADIR, filename, NULL);
}

/* display error dialog */
static void avs_message_dialog (const gchar* error1, const gchar* error2)
{
  GtkWidget *messdialog;

  messdialog = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_WARNING, GTK_BUTTONS_CLOSE, error1, NULL);
  gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (messdialog), error2, NULL);
  gtk_dialog_run (GTK_DIALOG (messdialog));
  gtk_widget_destroy (messdialog);
}

static void filemanager_avs_menu_provider_init (ThunarxMenuProviderIface *iface);

struct _FilemanagerAvsClass
{
  GObjectClass __parent__;
};

struct _FilemanagerAvs
{
  GObject __parent__;
};

THUNARX_DEFINE_TYPE_WITH_CODE (FilemanagerAvs, filemanager_avs, G_TYPE_OBJECT, THUNARX_IMPLEMENT_INTERFACE (THUNARX_TYPE_MENU_PROVIDER, filemanager_avs_menu_provider_init))

static void filemanager_avs_init (FilemanagerAvs *filemanageravs)
{
}

static void filemanager_avs_finalize (GObject *object)
{
	FilemanagerAvs *filemanageravs = FILEMANAGER_AVS (object);
}

static void filemanager_avs_class_init (FilemanagerAvsClass *klass)
{
	GObjectClass *gobject_class;
	
	gobject_class = G_OBJECT_CLASS (klass);
	gobject_class->finalize = filemanager_avs_finalize;
}

static void filemanager_avs_activated (ThunarxMenuItem *item, GList *filenames)
{
	gchar *path;
	gchar *cmd;
	gboolean retval;
	GError *error = NULL;
  GList *l = NULL;

  /* the command and file */
  cmd = g_build_filename ("/usr/bin/", "filemanager-avs", NULL);
  for (l = filenames; l != NULL; l = l->next)
  {
    cmd = g_strconcat (cmd, " ", l->data, " ", NULL);
  }

	/* make it so */
	retval = g_spawn_command_line_async (cmd, &error);
	if (retval == FALSE)
	{
		avs_message_dialog (gettext("Could not execute Filemanager-AVS"),  error->message);
	}
	g_free (cmd);
}

static GList* filemanager_avs_get_folder_menu_items (ThunarxMenuProvider *provider, GtkWidget *window, ThunarxFileInfo *folder)
{
  ThunarxMenuItem *item = NULL;
  GList *items = NULL;
  GList *filename = NULL;

  filename = g_list_prepend (filename, g_file_get_path (thunarx_file_info_get_location (folder)));
  item = thunarx_menu_item_new ("FilemanagerAvs::open-filemanager-avs-here", gettext ("Scan folder for viruses"), gettext ("Open Filemanager-AVS in this folder"), avs_get_full_location ("Filemanager-AVS.png"));
  g_signal_connect (G_OBJECT (item), "activate", G_CALLBACK (filemanager_avs_activated), filename);
  return g_list_prepend (items, item);
}

static GList* filemanager_avs_get_file_menu_items (ThunarxMenuProvider *provider, GtkWidget *window, GList *files)
{
  ThunarxMenuItem *item = NULL;
  GList *items = NULL;
  GList *filenames =  NULL;
  GList *l = NULL;

  for (l = files; l != NULL; l = l->next)
  {
    filenames = g_list_prepend (filenames, g_file_get_path (thunarx_file_info_get_location (l->data)));
  }
  if (G_LIKELY (files != NULL && files->next == NULL && thunarx_file_info_is_directory (files->data)))
  {
    return filemanager_avs_get_folder_menu_items (provider, window, files->data);
  }
  item = thunarx_menu_item_new ("FilemanagerAvs::open-filemanager-avs-here", gettext ("Scan file for viruses"), gettext ("Open Filemanager-AVS to scan this file"), avs_get_full_location ("Filemanager-AVS.png"));
  g_signal_connect (G_OBJECT (item), "activate", G_CALLBACK (filemanager_avs_activated), filenames);
  return g_list_prepend (items, item);
}

static void filemanager_avs_menu_provider_init (ThunarxMenuProviderIface *iface)
{
  iface->get_file_menu_items = filemanager_avs_get_file_menu_items;
  iface->get_folder_menu_items = filemanager_avs_get_folder_menu_items;
}
